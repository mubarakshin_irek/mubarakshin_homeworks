package com.company;

import java.util.*;

/*На вход подается строка с текстом. Необходимо посчитать, сколько встречается раз каждое слово в этой строке.

Вывести:
Слово - количество раз

Использовать Map, string.split(" ") - для деления текста по словам. Слово - символы, ограниченные пробелами справа и слева.
*/

public class Main {


    public static void main(String[] args) {
        System.out.println("Введите текст");
        Scanner scanner = new Scanner(System.in);
        String string = scanner.nextLine();
        String[] words = string.split("\\s+");
        HashMap<String, Integer> wordToCount = new HashMap<>();

        for (String word : words) {
            if (!wordToCount.containsKey(word)) {
                wordToCount.put(word, 0);
            }
            wordToCount.put(word, wordToCount.get(word) + 1);
        }
        for (String word : wordToCount.keySet()) {
            System.out.println("Слово - "+ word + " " + "количество раз " + wordToCount.get(word));
        }
        scanner.close();
    }
}

