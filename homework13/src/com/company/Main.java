package com.company;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        int[] array = {1,2,3,444,56,58,57,1111111111,73,18};
        int [] returnEvenDigitInMassive = Sequence.filter(array, number -> number % 2 == 0);
        System.out.println(Arrays.toString(returnEvenDigitInMassive));

        int [] returnSumEvenDigit = Sequence.filter2(array, number -> number % 2 == 0);
        System.out.println(Arrays.toString(returnSumEvenDigit));

    }
}
