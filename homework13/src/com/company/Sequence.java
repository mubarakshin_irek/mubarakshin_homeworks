package com.company;

public class Sequence {

    public static int[] filter(int[] array, ByCondition condition) {
          int evenNumber = 0;
        for (int i =0 ; i < array.length; i++){
            if (condition.isOk (array[i])){
                evenNumber++;
            }
        }
        int [] result = new int[evenNumber];
       int index = 0;
       for (int i =0 ; i < array.length; i++){
            if (condition.isOk (array[i])){
                result [index] = array[i];
                index++;
            }
        }
      return result;
    }
    public static int[] filter2(int[] array, ByCondition condition) {
        int [] result = new int [array.length];
        int evenNumber = 0;
        for (int i =0 ; i < array.length; i++){
            //result [i] = ((array[i] / 100) + (array[i] / 10 % 10) + (array[i] % 10) );
            int temp = array[i];
            int sum = 0;
            int remainder ;
            while (temp % 10 != 0){
                remainder = temp % 10;
                sum = sum+ remainder ;
                temp = temp/10;
            }
            if (condition. isOk(sum)) {
               evenNumber++;
           }
        }
        int [] result2 = new int[evenNumber];
        int index = 0;
        for (int i =0 ; i < array.length; i++){
            int temp = array[i];
            int sum = 0;
            int remainder ;
            while (temp % 10 != 0){
                remainder = temp % 10;
                sum = sum+ remainder ;
                temp = temp/10;
            }
            if (condition.isOk (sum)){
                result2 [index] = array[i];
                index++;
            }
        }
        return result2;
    }
}



