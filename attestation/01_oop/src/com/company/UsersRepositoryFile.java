package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class UsersRepositoryFile implements UsersRepository {

    private String fileName;

    public UsersRepositoryFile(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public List<User> findAll() {
        List<User> users = new ArrayList<>();
        Reader reader = null;
        BufferedReader bufferedReader = null;
        try {
            reader = new FileReader(fileName);
            bufferedReader = new BufferedReader(reader);
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] parts = line.split("\\|");
                int id = Integer.parseInt(parts[0]);
                String name = parts[1];
                int age = Integer.parseInt(parts[2]);
                boolean isWorker = Boolean.parseBoolean(parts[3]);
                User newUser = new User(id, name, age, isWorker);
                users.add(newUser);
                line = bufferedReader.readLine();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException ignore) {
                }
            }
        }
        return users;
    }

    @Override
    public void update(User user) {
        List<User> users = findAll();
        for (User u : users) {
            if (u.getId() == user.getId()) {
                users.remove(u);
                break;
            }
        }
        users.add(user);
        Writer writer = null;
        BufferedWriter bufferedWriter = null;
        try {
            writer = new FileWriter(fileName, false);
            bufferedWriter = new BufferedWriter(writer);
            for (User u : users) {
                bufferedWriter.write(u.getId() + "|" + u.getName() + "|" + u.getAge() + "|" + u.isWorker());
                bufferedWriter.newLine();
                bufferedWriter.flush();
            }

        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException ignore) {
                }
            }
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException ignore) {
                }
            }
        }
    }

    @Override
    public User findById(int id) {

        List<User> users = new ArrayList<>();
        for (User user : findAll()) {
            if (user.getId() == id) {
                return user;
            }
        }
        return null;

    }
}
