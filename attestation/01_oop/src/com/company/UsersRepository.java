package com.company;

import java.util.Collection;
import java.util.List;

public interface UsersRepository {

    User findById(int id);

    List<User> findAll();

    void update(User user);
}


