package com.company;

/*Реализовать в классе UsersRepositoryFileImpl методы:

User findById(int id);
update(User user);

Принцип работы методов:

Пусть в файле есть запись:

1|Игорь|33|true

Где первое значение - гарантированно уникальный ID пользователя (целое число).

Тогда findById(1) вернет объект user с данными указанной строки.

Далее, для этого объекта можно выполнить следующий код:

user.setName("Марсель");
user.setAge(27);

и выполнить update(user);

При этом в файле строка будет заменена на 1|Марсель|27|true.

Таким образом, метод находит в файле пользователя с id user-а и заменяет его значения.

Примечания:
Бесполезно пытаться реализовать замену данных в файле без полной перезаписи файла ;)

 */

import java.util.List;

public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFile("users.txt");


        User userById = usersRepository.findById(10);
        System.out.println("Выбранный user - " + userById);

        if (userById== null) {
            System.err.println("Пользователя с таким id не существует");
        }else {

            userById.setName("Тимур");
            userById.setAge(44);
            usersRepository.update(userById);

            List<User> updatedUsers = usersRepository.findAll();

            System.out.println("Измененный файл:");

        for (User user : updatedUsers) {
            System.out.println(user);
            }
        }

    }
}


