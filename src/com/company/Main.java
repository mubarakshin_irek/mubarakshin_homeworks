package com.company;

import java.util.Arrays;
import java.util.Comparator;

public class Main {


               public static void main(String[] args) {
                   Person p1 = new Person("Фёдор", 54);
                   Person p2 = new Person("Фарид", 78);
                   Person p3 = new Person("Василий", 65);
                   Person p4 = new Person("Андрей", 75);
                   Person p5 = new Person("Юра", 73);
                   Person p6 = new Person("Саша", 95);
                   Person p7 = new Person("Илья", 100);
                   Person p8 = new Person("Дима", 78);
                   Person p9 = new Person("Люда", 55);
                   Person p10 = new Person("Альбина", 50);



                   Person[] people = {p1, p2, p3, p5, p6, p7, p8, p9, p10};

                   int i;
                   for ( i=0; i< people.length; i++ ){
                       if (people[i].getWeight()< 0) {
                           people[i].setWeight(0);
                       }

                       Arrays.sort(people, Comparator.comparingInt(Person::getWeight));
                       System.out.println(people[i].toString());
                   }
               }
           }
