create table product
(
    id serial primary key,
    description varchar(20),
    price integer check (price>=0),
    amount integer check (amount>=0)
);

insert into product(description, price, amount) values ('SSD',69,58);
insert into product(description, price, amount) values ('RAM',69,211);
insert into product(description, price, amount) values ('GPU',969,8);
insert into product(description, price, amount) values ('GPU',1469,38);
insert into product(description, price, amount) values ('CPU',325,178);
insert into product(description, price, amount) values ('CPU',255,548);

create table customer
(
    id serial primary key,
    name varchar(20)
);

insert into customer(name) values ('Рустем');
insert into customer(name) values ('Наиль');
insert into customer(name) values ('Марсель');
insert into customer(name) values ('Айдар');


create table booking
(
    id serial primary key,
    date DATE,
    amount integer check (amount>0),
    customer_id integer,
    foreign key (customer_id) references customer(id),
    product_id integer,
    foreign key (product_id) references product(id)
);

insert into booking(date, amount, customer_id, product_id) values ('2021-11-20',2,2,2);
insert into booking(date, amount, customer_id, product_id) values ('2021-10-20',10,1,1);
insert into booking(date, amount, customer_id, product_id) values ('2021-11-12',3,4,4);
insert into booking(date, amount, customer_id, product_id) values ('2021-10-20',1,3,1);
insert into booking(date, amount, customer_id, product_id) values ('2021-11-20',1,2,6);
insert into booking(date, amount, customer_id, product_id) values ('2021-11-25',12,2,4);
insert into booking(date, amount, customer_id, product_id) values ('2021-11-26',5,4,3);


-- получить имена закзчиков
select name
from customer;

-- получить имя пользователя с id 4 и количество его заказов
select name, (select count(*) from booking where customer_id= 4) as orders
from customer
where id = 4;

-- получить имена и количество заказанных продуктов
select name, (select count(*) from booking where customer_id  = customer.id)
from customer;

-- Покупатель, потративший наибольшую сумму
select name as Customer, sum(o.amount * price) as Purchase_amount
from customer
         inner join booking o on customer.id = o.customer_id
         inner join product p on p.id = o.product_id
group by name
order by Purchase_amount desc
    limit 1;

-- В каком количестве, когда и какой зазчик заказл товар
select *from product inner join booking on product.id = booking.product_id;



