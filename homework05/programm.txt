package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int minDigit = 9;
           while (a != -1){
                    while (a != 0){
                        int lastNumber = (a % 10);
                        if (lastNumber <= minDigit){
                            minDigit = lastNumber;}
                        a = a/10;
                        }
               a = scanner.nextInt();
           }
        System.out.println(minDigit);
}
}