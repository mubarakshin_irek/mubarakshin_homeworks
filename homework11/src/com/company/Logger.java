package com.company;

public class Logger {
    private   String message;

    private static final Logger INSTANCE = new Logger();

    private Logger(){}

    public static Logger getInstance(){
        return INSTANCE;
    }

    public static String log(String message) {
        System.out.println (message);
        return message;
    }
}


