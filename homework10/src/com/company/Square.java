package com.company;

public class Square extends Rectangle implements Shift {
    public Square(int x1, int y1) {
        super(x1, y1);
    }
    @Override
    public void getShift(int x1, int y1) {
        this.x1 = x1;
        this.y1 = y1;

    }

    @Override
    public String toString() {
        return "Square{" +
                "x1=" + x1 +
                ", y1=" + y1 +
                '}';
    }
}





