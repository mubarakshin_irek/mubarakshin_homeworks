package com.company;

public abstract class Figure  {

    protected int x1;
    protected int y1 ;


    public Figure(int x1, int y1) {
        this.x1 = x1;
        this.y1 = y1;
    }
    public int getX(){
        return x1;
    }

    public int getY1() {
        return y1;
    }

    public void setX1(int x1) {
        this.x1 = x1;
    }

    public void setY1(int y1) {
        this.y1 = y1;
    }
}


