package com.company;

/*Подготовить файл с записями, имеющими следующую структуру:
[НОМЕР_АВТОМОБИЛЯ][МОДЕЛЬ][ЦВЕТ][ПРОБЕГ][СТОИМОСТЬ]

o001aa111|Camry|Black|133|82000
o002aa111|Camry|Green|133|0
o001aa111|Camry|Black|133|82000

Используя Java Stream API, вывести:

Номера всех автомобилей, имеющих черный цвет или нулевой пробег. // filter + map
Количество уникальных моделей в ценовом диапазоне от 700 до 800 тыс. // distinct + filter
* Вывести цвет автомобиля с минимальной стоимостью. // min + map
* Среднюю стоимость Camry *

https://habr.com/ru/company/luxoft/blog/270383/

Достаточно сделать один из вариантов.
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


public class Main {
    public static void main(String[] args) {
        try (BufferedReader reader = new BufferedReader(new FileReader("input.txt"))) {
            reader.lines()
                    .map(line -> line.split("\\|"))
                    .filter(line -> line[2].equals("Black") || line[4].equals("0"))
                    .forEach(line -> System.out.println(line[0]));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}

