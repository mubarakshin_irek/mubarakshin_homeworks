package com.company;

public class Main {

    public static void main(String[] args) {

        Figure figure = new Figure( 30, 40) ;
        Ellipse ellipse = new Ellipse(4,3);
        Circle circle = new Circle(10);
        Rectangle rectangle = new Rectangle(7,4);
        Square square = new Square(8);


        System.out.println("Периметр фигуры -" +" "+ figure.getPerimeter());
        System.out.println ("Периметр эллипса -" +" "+ ellipse.getPerimeter());
        System.out.println ("Периметр круга -" +" "+ circle.getPerimeter());
        System.out.println ("Периметр прямоугольника -" +" "+ rectangle.getPerimeter());
        System.out.println ("Периметр квадрата -" +" "+ square.getPerimeter());
    }
}
