package com.company;

public class Ellipse extends Figure {
    public Ellipse(int t, int p) {
        super(t, p);
    }

    public double getPerimeter (){
        return (4*((3.14*x*y)+((x-y)*(x-y)))/(x+y));
    }
}

