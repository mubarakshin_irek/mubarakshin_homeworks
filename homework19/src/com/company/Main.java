package com.company;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        UsersRepository usersRepository = new UsersRepositoryFileImpl("users.txt");
        List<User> usersByAge = usersRepository.findByAge(27);

        for (User user : usersByAge) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }

        System.out.println("---------");

        List<User> usersIsWorkerIsTrue = usersRepository.findByIsWorkerIsTrue();
        for (User user : usersIsWorkerIsTrue) {
            System.out.println(user.getAge() + " " + user.getName() + " " + user.isWorker());
        }
    }
}


